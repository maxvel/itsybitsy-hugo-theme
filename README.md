# ItsyBitsy Hugo Theme
ItsyBitsy hugo theme (itsybitsy because it's and bits in the name sounded cool) focuses on leveraging the content by focusing on minimalistic design. This theme is primarily focused on blogging. Goal of this theme is to make the theme minimalistic enough to not be boring.

## How to add
You can use this theme just like you use any other hugo theme.
1. `cd <ProjectDirectory>`
2. `git clone https://codeberg.org/maxvel/itsybitsy-hugo-theme.git themes/itsybitsy`

That's it. Theme will be cloned to themes directory.

## How to use
You can either use this theme via Terminal or `config.yml` file.
### Terminal
1. `hugo -t itsybitsy` will generate the static files with the theme. OR;
2. Add `theme = itsybitsy` to your `config.toml` file (Add `theme: itsybitsy` if `config.yml`) And then run hugo in the terminal.

## Things to Consider
1. Modify the content in `<Project>/themes/layouts/index.html` to your needs
2. Create a directory in `content/subscribe` to customize subscription section (RSS).
3. Create a directory in `content/categories` to customize categories section.
4. All posts will reside in `content/posts`. (You can add a new post by entering command `hugo new content/posts/<postname>.md`)
